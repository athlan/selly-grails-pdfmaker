package com.selly.controller

import com.selly.util.gsp.RendererUtil
import com.selly.util.pdfrenderer.PDFRendererUtil;

class GeneratePDFController {

    RendererUtil rendererUtil
	PDFRendererUtil pdfRendererUtil

	def index() {
		def files = []
		def file = new File("grails-app/views/document").eachFileRecurse {
	        if (it.file) {
				def fileName = it.getName();
				fileName = fileName.substring(0, fileName.lastIndexOf("."))
				
				files << fileName
			}
		}
		
		return [
			files: files
		]
	}
	
    def showFile() {
		OutputStream os = response.outputStream
		
		String fileName = params.filename ?: 'test-pdf'
		String file = "/document/" + fileName
		
		String contents = rendererUtil.getRenderer().render view: file, model: [date: new Date()]
		
		// if contents is empty that means, the file does not exists
		if(contents.equals(""))
			throw new Exception("No file: " + file)
		
		pdfRendererUtil.renderDocument(contents, os)
        
        os.close();
	}
}
