package com.selly.util.pdfrenderer;

import java.io.*;
import java.util.Date;

import com.lowagie.text.DocumentException;

import org.xhtmlrenderer.pdf.ITextRenderer;

@SuppressWarnings("deprecation")
public class PDFRendererUtil {
	
	public void renderDocument(String contents, OutputStream os) throws IOException, DocumentException {
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(contents);
        renderer.layout();
        renderer.createPDF(os);
        
        os.close();
	}
}
