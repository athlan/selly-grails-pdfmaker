// Place your Spring DSL code here
import com.selly.util.gsp.RendererUtil;
import com.selly.util.pdfrenderer.PDFRendererUtil;

beans = {
	rendererUtil(RendererUtil) {
		renderer = ref('groovyPageRenderer')
	}

	pdfRendererUtil(PDFRendererUtil)
}
